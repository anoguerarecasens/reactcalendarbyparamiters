import "./App.css";

const any = 1996;
const mes = 3;
var counterDay = 0;
function Dia(props) {
  counterDay++;

  const estyle = {
    color: isWeekend(counterDay) ? "red" : "black",
    fontWeight: "bolder",
  };

  if (props.valor === "") {
    return <div className="divDia" style={{ visibility: "hidden" }}></div>;
  }

  return (
    <>
      <div className="divDia" style={estyle}>
        {props.valor}
      </div>
    </>
  );

  function isWeekend(counterDay) {
    return counterDay % 7 === 5 || counterDay % 7 === 0;
  }
}

function Setmana(props) {
  const valors = props.dies.map((el, idx) => <Dia key={idx} valor={el} />);
  return <>{valors}</>;
}

function Calendari({ any, mes }) {
  const primerDia = new Date(any, mes - 1, 1);
  const ultimDia = new Date(any, mes, 0);
  var diaSetmana = primerDia.getDay();
  const diesTotals = ultimDia.getDate();

  const diesSetmana = getNomsDiv();
  const mesos = getNomMesos();

  if (diaSetmana === 0) diaSetmana = 7;

  const llistaDiesAmbBlancs = [];
  var lastPos;

  for (let blankPos = 1; blankPos < diaSetmana; blankPos++) {
    llistaDiesAmbBlancs.push("");
    lastPos = blankPos;
  }

  let dia = 1;
  while (lastPos < 7) {
    llistaDiesAmbBlancs.push(dia);
    dia++;
    lastPos++;
  }

  const setmanes = [];

  setmanes.push(<Setmana dies={llistaDiesAmbBlancs} />);

  setmanes.push(setTheRestOfDays(dia, diesTotals));

  return (
    <>
      {mesos[mes - 1]}
      <div className="divCalendari">
        {diesSetmana}
        {setmanes}
      </div>
    </>
  );
}

function getNomsDiv() {
  return [
    <div className="divDiesSetmana">
      <h4>Dl</h4>
    </div>,
    <div className="divDiesSetmana">
      <h4>Dm</h4>
    </div>,
    <div className="divDiesSetmana">
      <h4>Dc</h4>
    </div>,
    <div className="divDiesSetmana">
      <h4>Dj</h4>
    </div>,
    <div className="divDiesSetmana">
      <h4>Dv</h4>
    </div>,
    <div className="divDiesSetmana" style={{ color: "red" }}>
      <h4>Ds</h4>
    </div>,
    <div className="divDiesSetmana" style={{ color: "red" }}>
      <h4>Dg</h4>
    </div>,
  ];
}

function getNomMesos() {
  return [
    <h2>Gener</h2>,
    <h2>Febrer</h2>,
    <h2>Març</h2>,
    <h2>Abril</h2>,
    <h2>Maig</h2>,
    <h2>Juny</h2>,
    <h2>Juliol</h2>,
    <h2>Agost</h2>,
    <h2>Septembre</h2>,
    <h2>Octubre</h2>,
    <h2>Novembre</h2>,
    <h2>Desembre</h2>,
  ];
}

function setTheRestOfDays(dia, diesTotals) {
  let counterDies = 0;
  var llistaDies = [];
  const retorn = [];

  while (dia <= diesTotals) {
    if (counterDies === 7) {
      retorn.push(<Setmana dies={llistaDies} />);
      counterDies = 0;
      llistaDies = [];
    }
    llistaDies.push(dia);
    counterDies++;
    dia++;
  }

  if (llistaDies.length !== 0) retorn.push(<Setmana dies={llistaDies} />);

  return retorn;
}

function App() {
  return <Calendari any={any} mes={mes} />;
}

export default App;
